#!/bin/bash

function create_groups() {
    local groups=$(tr ',' ' ' <<< ${1})

    for group in ${groups} ; do
        groupadd ${group} &> /dev/null
    done
}

# TODO. Los usuarios deben pertenecer a un grupo principal, podemos
# definirlo aquí o utilizar el primero de la lista
grupo_principal="users"
shell_usuario="/bin/bash"
fichero_usuarios="${1}"

# Comprobar si se ha ѕuministrado argumento con nombre de fichero
if [ -z ${fichero_usuarios} ] ; then
    echo "Uso: ${0} <ruta_a_fichero>"
    exit 1
fi

# Comprobar si eres el root
if [ `id -u` -ne 0 ] ; then
    echo "No eres root, no puedes continuar"
    exit 2
fi

# Comprobar si el fichero existe
if [ -f ${fichero_usuarios} ] ; then
    echo "Obteniendo usuarios del fichero"
else
    echo "El fichero no existe."
    exit 3
fi

usuarios=`cat ${fichero_usuarios} | tr " " _ | sed "1 d"`

for i in $usuarios ; do
    login=`echo $i | cut -d: -f1` # nombre de usuario
    campo1=`echo $i | cut -d: -f2` # nombre
    contra=`echo $i | cut -d: -f3` # contraseña
    grupo=`echo $i | cut -d: -f4` # grupo
    create_groups ${grupo}
    useradd -m \
        -s ${shell_usuario} \
        -c ${campo1} \
        -p $(mkpasswd -m sha-256 $contra) \
        -g ${grupo_principal} \
        -G ${grupo} \
        ${login}
done
